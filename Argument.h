//
// Created by phucn on 10/1/2020.
//

#ifndef DC_ARGUMENT_H
#define DC_ARGUMENT_H

#include <string>
#include <sstream>
#include "Common.h"

using namespace std;

class Argument {
public:
    int mode = 5;
    int format = 0;
    string dataDir = "../data/";
    string graph = "facebook";
    int threads = 10;
    int noc = 8;
    int npc = 20;

    int nc = 0;
    int rb = 0;
    int rt = 0;

    double thresholdPct = 0.5;
    double epsilon = 0.2;
    double delta = 0.2;
    string outPath = "";

    Argument() = default;

    ~Argument() = default;

    Argument(int argc, const char *argv[]) {
        Common::mkdir_absence(outPath.c_str());
        string param, value;
        for (int i = 1; i < argc; ++i) {
            if (argv[i][0] != '-') break;
            stringstream sstr(argv[i]);
            getline(sstr, param, '=');
            getline(sstr, value, '=');
            if (!param.compare("-m")) mode = stoi(value);
            else if (!param.compare("-t")) threads = stoi(value);
            else if (!param.compare("-noc")) noc = stoi(value);
            else if (!param.compare("-npc")) npc = stoi(value);
            else if (!param.compare("-nc")) nc = stoi(value);
            else if (!param.compare("-rb")) rb = stoi(value);
            else if (!param.compare("-rt")) rt = stoi(value);
            else if (!param.compare("-f")) format = stoi(value);
            else if (!param.compare("-tp")) thresholdPct = stod(value);
            else if (!param.compare("-e")) epsilon = stod(value);
            else if (!param.compare("-d")) delta = stod(value);
            else if (!param.compare("-dd")) dataDir = value;
            else if (!param.compare("-g")) graph = value;
            else if (!param.compare("-o")) {
                outPath += value + "/";
                Common::mkdir_absence(outPath.c_str());
            }
        }

    }
};


#endif //DC_ARGUMENT_H
