#pragma once

class Constant {
public:
    Constant();

    ~Constant();

    static int K;
    static bool IS_WEIGHTED;
    static int COMMUNITY_POPULATION;
    static double PERCENTAGE_THRESHOLD;
    static double EPSILON;
    static double DELTA;
    static int NUM_THREAD;
    static int mode;
    static int noc;
    static int npc;
    static bool IS_BOUNDED_THRESHOLD;
    static bool MODEL; // true : LT ; false : IC
};

