//
// Created by phucn on 10/1/2020.
//

#ifndef DC_FORMATTER_H
#define DC_FORMATTER_H

#include <iostream>
#include <fstream>
#include "map"
#include "Argument.h"
#include "StopWatch.h"
#include "SFMT.h"

using namespace std;

class Formatter {
private:
    Argument arg;
    StopWatch *sw = new StopWatch();
    int numNodes = 0;
    int numEdges = 0;
    vector<int> srcNodeList, dstNodeList;
    map<int, bool> nodeIdList;
    map<int, vector<int>> mapDstToIncomingNodes;
    map<int, vector<int>> mapSrcToOutgoingNodes;
    map<int, double> mapDstToWeight;
    map<int, int> inDegree;
    map<int, int> outDegree;
    sfmt_t sfmtSeed;

public:
    Formatter() = default;

    ~Formatter() = default;

    Formatter(Argument
              pArg) {
        arg = pArg;
        sfmt_init_gen_rand(&sfmtSeed, rand());
    }

    void run() {
        sw->start();
        cout << "generating edge weights..." << endl;
        genWeight();
        cout << "converting to binary file..." << endl;
        convertToBinary();
        sw->stop();
        cout << "runtime: " << sw->getSeconds() << "s" << endl;
    }

    void genWeight() {
        string ifPath = arg.dataDir + "/" + arg.graph + ".txt";
        string ofPath = arg.dataDir + "/" + arg.graph + ".adj";
        int srcNode, dstNode;

        ifstream in(ifPath);
        while (in >> srcNode >> dstNode) {
            numEdges++;
            srcNodeList.push_back(srcNode);
            dstNodeList.push_back(dstNode);

            // unique node is list
            if (!nodeIdList[srcNode]) {
                nodeIdList[srcNode] = true;
            }
            if (!nodeIdList[dstNode]) {
                nodeIdList[dstNode] = true;
            }

            // save incoming node & indegree of each dstNode
            if (mapDstToIncomingNodes.find(dstNode) != mapDstToIncomingNodes.end()) {
                mapDstToIncomingNodes[dstNode].push_back(srcNode);
                inDegree[dstNode]++;
            } else {
                mapDstToIncomingNodes.insert(make_pair(dstNode, vector < int > {srcNode}));
                inDegree.insert(pair<int, int>(dstNode, 1));
            }

            // save outgoing node & outdegree of each srcNode
            if (mapSrcToOutgoingNodes.find(srcNode) != mapSrcToOutgoingNodes.end()) {
                mapSrcToOutgoingNodes[srcNode].push_back(dstNode);
                outDegree[srcNode]++;
            } else {
                mapSrcToOutgoingNodes.insert(make_pair(srcNode, vector < int > {dstNode}));
                outDegree.insert(pair<int, int>(srcNode, 1));
            }
        }
        in.close();
        numNodes = nodeIdList.size();

        // maps dstNode to edge weight
        for (auto incomingNodes : mapDstToIncomingNodes) {
            double weight = (double) 1 / incomingNodes.second.size();
            mapDstToWeight[incomingNodes.first] = weight;
        }

        // writing to txt file
        ofstream out(ofPath);
        for (int i = 0; i < numEdges; ++i) {
            double w = mapDstToWeight[dstNodeList[i]];
            out << srcNodeList[i] << " " << dstNodeList[i] << " " << w << endl;
        }
        out.close();
    }

    void convertToBinary() {
        string ofPath = arg.dataDir + "/" + arg.graph + ".graph";
        ofstream out(ofPath, fstream::out | fstream::binary);

        // outputs number of nodes
        out.write((char *) &numNodes, sizeof(int));

        // outputs number of edges
        out.write((char *) &numEdges, sizeof(int));

        // outputs node id list
        for (auto nodeId : nodeIdList) {
            int id = nodeId.first;
            out.write((char *) &id, sizeof(int));
        }

        // outputs weights of incoming for dstNode
        for (auto nodeId : nodeIdList) {
            double weight = 0;
            if (inDegree.find(nodeId.first) != inDegree.end()) {
                weight = mapDstToWeight[nodeId.first];
            }
            out.write((char *) &weight, sizeof(double));
        }

        // outputs node's cost
        map<int, double> mapNodeCost;
        for (auto nodeId : nodeIdList) {
            double cost = 1;
            if (outDegree.find(nodeId.first) != outDegree.end()) {
                cost = ((double) numNodes * outDegree[nodeId.first]) / numEdges;
            }
            mapNodeCost[nodeId.first] = cost;
            out.write((char *) &cost, sizeof(double));
        }

        // outputs node's benefit
        for (auto nodeId : nodeIdList) {
            double benefit = sfmt_genrand_real1(&sfmtSeed);
            // double benefit = sfmt_genrand_real1(&sfmtSeed) + mapNodeCost[nodeId.first];
            out.write((char *) &benefit, sizeof(double));
        }

        // outputs cumulative indegree sequence
        for (auto nodeId : nodeIdList) {
            int degree = 0;
            if (inDegree.find(nodeId.first) != inDegree.end()) {
                degree = inDegree[nodeId.first];
            }
            out.write((char *) &degree, sizeof(int));
        }

        // outputs incoming of dstNode
        for (auto nodeId : nodeIdList) {
            int degree = 0;
            vector<int> inNodes(degree);
            if (inDegree.find(nodeId.first) != inDegree.end()) {
                degree = inDegree[nodeId.first];
                inNodes = mapDstToIncomingNodes[nodeId.first];
            }
            if (degree > 0) {
                out.write((char *) &inNodes[0], degree * sizeof(int));
            }
        }

        // outputs cumulative outdegree sequence
        for (auto nodeId : nodeIdList) {
            int degree = 0;
            if (outDegree.find(nodeId.first) != outDegree.end()) {
                degree = outDegree[nodeId.first];
            }
            out.write((char *) &degree, sizeof(int));
        }

        // outputs outgoing of dstNode
        for (auto nodeId : nodeIdList) {
            int degree = 0;
            vector<int> outNodes(degree);
            if (outDegree.find(nodeId.first) != outDegree.end()) {
                degree = outDegree[nodeId.first];
                outNodes = mapSrcToOutgoingNodes[nodeId.first];
            }
            if (degree > 0) {
                out.write((char *) &outNodes[0], degree * sizeof(int));
            }
        }
        out.close();
    }
};


#endif //DC_FORMATTER_H
