#pragma once

#include <iostream>
#include <vector>
#include <omp.h>
#include <string>
#include <fstream>

#ifdef _WIN32

#include <windows.h>

#else
#include <sys/stat.h>
#endif

using namespace std;

class Common {
public:
    Common();

    ~Common();

    template<class T>
    static string getMethodName(T arg) {
        if (arg.mode == 1) {
            return "MIP";
        } else if (arg.mode == 2) {
            return "AIG";
        } else if (arg.mode == 3) {
            return "UBG";
        } else if (arg.mode == 4) {
            return "HighDegree";
        } else if (arg.mode == 5) {
            return "OPIM";
        }
        return "";
    }

    template<typename T>
    static void printVector(vector<T> vt, ostream &stream = cout) {
        if (vt.empty()) return;
        stream << "<" << vt[0];
        for (int i = 1; i < vt.size(); ++i) {
            stream << " " << vt[i];
        }
        stream << ">" << endl;
    }

    template<class T>
    static void recordResult(T arg, double cost, double runtime) {
        string outPath = arg.outPath + arg.graph + ".csv";
        bool header = true;
        struct stat buffer;
        if (stat(outPath.c_str(), &buffer) == 0) {
            header = false;
        }
        ofstream out(outPath, fstream::out | fstream::app);
        if (header) {
            out
                    << "graph,method,cost,runtime,epsilon,delta,noOfComms,nodesPerComm,thresholdPct,normalCost,randomBenefit,randomThresholdPct"
                    << endl;
        }
        out << arg.graph << "," << getMethodName(arg) << "," << cost << "," << runtime << "," << arg.epsilon << ","
            << arg.delta << "," << arg.noc << "," << arg.npc << "," << arg.thresholdPct << "," << arg.nc << ","
            << arg.rb << "," << arg.rt << endl;
        out.close();
    }

    static Common *getInstance();

    unsigned nChoosek(unsigned n, unsigned k);

    bool isIntersected(vector<int> *set1, vector<int> *set2); // both 2 set is sorted
    vector<int> setDifference(vector<int> *set1, vector<int> *set2);

    unsigned randomInThread();

    static void mkdir_absence(const char *outFolder) {
#if defined(_WIN32)
        CreateDirectoryA(outFolder, nullptr); // can be used on Windows
#else
        mkdir(outFolder, 0733); // can be used on non-Windows
#endif
    }

    template<typename T>
    static void sortVector2DBySize(vector<vector<T>> &vt2d, bool decreased = true) {
        if (decreased) {
            sort(vt2d.begin(), vt2d.end(), [](const vector<int> &a, const vector<int> &b) {
                return a.size() > b.size();
            });
        } else {
            sort(vt2d.begin(), vt2d.end(), [](const vector<int> &a, const vector<int> &b) {
                return a.size() < b.size();
            });
        }
    }

    static unsigned long long nCk(int n, int k) {
        if (k > n)
            return 0;
        unsigned long long r = 1;
        for (int d = 1; d <= k; d++) {
            r *= n--;
            r /= d;
        }
        return r;
    }

private:
    static Common *instance;
    int *seed;
};

