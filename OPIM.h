#pragma once

#include "stdafx.h"
#include "Algorithm.h"

class OPIM : public Algorithm {
public:
    OPIM(SocialGraph *g);

    OPIM(Argument arg, SocialGraph *g);

    ~OPIM();

    double getDeterministicSolutionSpeed(vector<int> *sol);

    double getDeterministicSolution(vector<int> *sol);

    //double estimate(vector<int> * sol, double epsilon2, double delta, int tMax);
    double getSolution(vector<int> *sol, double *est);

    double getSolution2Step(vector<int> *sol, double *est);

    double estimate(vector<int> *sol, double epsilon, double delta, int tMax);

    double estimate(vector<int> *sol);

    void initiate();

    void formatFile();

    double binarySearchSolution(vector<int> *sol, double *est, double left, double right);

private:
    Argument arg;
    Graph graph;
    TAlg *tAlg;
    double ep;
    double delta;
    double initR;
    double maxR;
    int iMax;
    double delta1;
    double c;
    vector<int> finalSeeds;
    double finalInfluenced;

    double getMarginalGain(int nodeId, vector<int> *sol);

    double getMarginalGainMig(int nodeId, vector<int> *sol);

    vector<vector<int>> currentLive; // store current live node in each dcr graph after each iteration in greedy
    vector<double> currentBenefit;
};
