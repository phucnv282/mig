#include "MIP.h"
#include <omp.h>
#include <iostream>
#include <ilcplex/ilocplex.h>


MIP::MIP(SocialGraph *g) : Algorithm(g) {

}

MIP::~MIP() {
}

/*official running*/
double MIP::getDeterministicSolution(vector<int> *sol) {
    int numOfNodes = g->getNumberOfNodes();
    vector<int> *listNodeIds = g->getListNodeIds();

    IloEnv env;
    try {
        IloModel model(env);

        // variables
        stringstream name;
        IloNumVarArray x(env, numOfNodes);
        for (int i = 0; i < numOfNodes; ++i) {
            name << "x_" << i;
            x[i] = IloNumVar(env, 0, 1, ILOINT, name.str().c_str());
            name.str("");
        }

        // constraints
        for (int i = 0; i < dcrSet.size(); ++i) {
            RICGraph *ric = dcrSet[i];
            map<int, vector<int>> *mapReachable = ric->getMapReachable();
            IloExpr gain(env);
            IloNumVarArray z(env, mapReachable->size(), 0, 1, ILOINT);
            int counter = 0;
            for (const auto &it : *mapReachable) {
                int dstNode = it.first;
                vector<int> reachableNodes = it.second;
                IloExpr sum(env);
                for (int j = 0; j < reachableNodes.size(); ++j) {
                    int reachableNode = reachableNodes[j];
                    sum += x[mapNodeIdx[reachableNode]];
                }
                model.add(IloIfThen(env, sum >= 1, z[counter] == 1));
                model.add(IloIfThen(env, sum < 1, z[counter] == 0));
                // gain += z[counter];
                gain += z[counter] * g->getNodeBenefit(dstNode); // benefit
                sum.end();
                counter++;
            }
            model.add(gain >= ric->getDThreshold());
            gain.end();
        }

        // objective
        IloExpr obj(env);
        for (int i = 0; i < numOfNodes; ++i) {
            // obj += x[i];
            obj += x[i] * g->getNodeCost(listNodeIds->at(i)); // cost
        }
        model.add(IloMinimize(env, obj));
        obj.end();

        // solve
        IloCplex cplex(model);
        // cplex.setOut(env.getNullStream());
        // cplex.setParam(IloCplex::TiLim, 3600);
        cplex.setParam(IloCplex::Threads, 10);
        cplex.solve();

        // post optimization
        // construct seeds
        for (int i = 0; i < numOfNodes; ++i) {
            if (cplex.isExtracted(x[i]) && cplex.getValue(x[i]) >= 0.5) {
                sol->push_back(listNodeIds->at(i));
            }
        }
    } catch (IloException &e) {
        cerr << "Conver exception caught: " << e << endl;
    } catch (...) {
        cerr << "Unknown exception caught" << endl;
    }
    env.end();
    return g->getNumberOfCommunities();
}

double MIP::getSolution(vector<int> *sol, double *est = 0) {
    initiate();
    omp_set_num_threads(Constant::NUM_THREAD);
    generateDCRgraphsMig((int) initR);
    for (int i = 0; i < 1; ++i) {
        sol->clear();
        getDeterministicSolution(sol);
        *est = estimate();
        double K = (double) g->getNumberOfCommunities();
        cout << "#seeds: " << sol->size() << " - #estimate: " << *est << endl;
        if (*est >= K - ep * K) {
            return 0;
        } else {
            generateDCRgraphsMig(dcrSet.size());
        }
    }
    clear();
    return 0;
}

double MIP::getSolution2Step(vector<int> *sol, double *est) {
    sol->clear();
    initiate();
    omp_set_num_threads(Constant::NUM_THREAD);
    generateDCRgraphs((int) rMax);
    double re = getDeterministicSolution(sol);
    *est = estimateInf(sol);
    clear();
    return (*est) / re;
}


double MIP::estimate(vector<int> *sol, double epsilon, double delta, int tMax) {
    double lamda = 0.72;
    double tmp = 4 * lamda * log(2 / delta) / (epsilon * epsilon);
    double lambda = 1 + (1 + epsilon) * tmp;

    int T = 0;
    double inf = 0.0;

#pragma omp parallel for
    for (int i = 0; i < tMax; i++) {
        RICGraph *g = gen.generateDCRgraph();
        double fr = g->fractionalInf(sol);

#pragma omp critical
        {
            dcrSet.push_back(g);
            g->updateInitalGain(&intialGain, &initialDead);
            if (tMax > 0) {
                T++;
                inf += fr;
                if (inf >= lambda) {
                    tMax = -1;
                }
            }
        }
    }

    return (tMax == -1 ? lambda * (Constant::IS_WEIGHTED ? g->getNumberOfNodes() : g->getNumberOfCommunities()) / T
                       : -1);
}


double MIP::getMarginalGain(int nodeId, vector<int> *sol) {
    double re = 0.0;
    for (int i = 0; i < dcrSet.size(); i++) {
        RICGraph *dcr = dcrSet[i];
        int gain = dcr->getMarginalGain(nodeId, &(currentLive[i]));
        re += ((double) gain) / dcr->getThreshold();
    }
    return re;
}

void MIP::initiate() {
    ep = Constant::EPSILON;
    delta = Constant::DELTA;
    int n = g->getNumberOfNodes();
    int kmax = n / 2;
    initR = ((2 + ((2 * ep) / 3)) / (ep * ep)) * log(1 / delta);
    unsigned long long nCkmax = Common::nCk(n, kmax);
    maxR = ((2 + ((2 * ep) / 3)) / (ep * ep)) * log((2 * nCkmax) / delta);
    iMax = ceil(log2(maxR / initR));
    delta1 = delta / (2 * iMax);
    c = log(1 / delta1);

    vector<int> *nodeIds = g->getListNodeIds();
    indx = vector<int>(nodeIds->size(), 0);
    for (int i = 0; i < nodeIds->size(); i++) {
        int u = (*nodeIds)[i];
        indx[i] = i;
        mapNodeIdx.insert(pair<int, int>(u, i));
    }
}

double MIP::estimate() {
    double K = (double) g->getNumberOfCommunities();
    double T = (double) dcrSet.size();
    double hatSigma = K;

    double min1 = K - ((K * c) / (3 * T));
    double min2 = K + (K / T) * (((2 * c) / 3) - sqrt(((4 * c * c) / 9) + (2 * T * c * (hatSigma / K))));
    return min(min1, min2);
}
