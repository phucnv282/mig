#pragma once

#include<vector>
#include<map>
#include "SocialGraph.h"

using namespace std;

class RICGraph {
public:
    RICGraph(int communityId, int threshold, vector<int> *communityNodeIds);

    RICGraph(SocialGraph *g, int communityId, double dThreshold, vector<int> *communityNodeIds, double totalBenefit);

    ~RICGraph();

    void addReachable(int nodeId, vector<int> *reachNodeIds);

    map<int, vector<int> > *getMapReachable();

    int getPopulation();

    vector<int> *getCommunityNodeIds();

    vector<int> *getReachableIds(int nodeId);

    int getThreshold();

    double getDThreshold();

    int getCommunityId();

    vector<int> getCurrentLive(vector<int> *sol); // get current unreached node id from current solution
    int getMarginalGain(int nodeId, vector<int> *currentLive);

    double getMarginalGainMig(int nodeId, vector<int> *currentLive, double *currentBenefit);

    void getCurrentLiveAfterAddingNode(int nodeId, vector<int> *currentLive);

    void getCurrentLiveAfterAddingNodeMig(int nodeId, vector<int> *currentLive, double *currentBenefit);

    // return map <nodeId, reduced in its marginal gain>, used for sandwich solution
    map<int, int> updateGainAndCurrentLiveAfterAddingNode(int nodeId, vector<int> *currentLive);

    map<int, double>
    updateGainAndCurrentLiveAfterAddingNodeMig(int nodeId, vector<int> *currentLive, double *currentBenefit);

    // return map <nodeId, reduced in its marginal gain>, used for compare greedy solution
    map<int, bool>
    updateGainAndCurrentLiveAfterAddingNodeCG(int nodeId, vector<int> *currentLive, vector<int> *canKill);


    void updateInitalGain(map<int, double> *mapInf, map<int, int> *mapDead = nullptr,
                          map<int, double> *mapInfMaf = nullptr); // used to update intial Gain when new dcrGraph generated
    void updateInitalGainMig(map<int, double> *mapInf, map<int, double> *greedy, map<int, int> *mapDead = nullptr,
                             map<int, double> *mapInfMaf = nullptr); // used to update intial Gain when new dcrGraph generated
    bool isKill(vector<int> *sol); // return true if sol can reach > threshold number of node in community
    bool isKillMig(vector<int> *sol); // return true if sol can reach > threshold number of node in community
    double fractionalInf(vector<int> *sol); // return fractional value of being influenced, using in sandwich solution

    bool isTouchedByNode(int nodeId);

    vector<int> *getListTouchedNode();

    vector<int> *getListTouchedNode(int excludeId);

    void initiateTrackGain();

    void initiateTrackGainMig();

    map<int, int> *getTrackGain();

    vector<int> *getKeyNodes();

private:
    SocialGraph *g;
    int communityId;
    int threshold;
    double dThreshold;
    double totalBenefit;
    vector<int> *communityNodeIds; // store id of nodes in community
    map<int, vector<int>> mapReachable; // store id (id of community nodes) -> list of node ids that can reach this node
    map<int, vector<int>> mapTouch; // store node id -> list of node ids that in community of dcr graph that this node can touch

    map<int, int> trackGain; // store node id -> gain (int type) of that node to this dcr graph , USED FOR SANDWICH SOLUTION
    map<int, double> dTrackGain; // store node id -> gain (int type) of that node to this dcr graph , USED FOR SANDWICH SOLUTION
    vector<int> keyNode; // store nodes that can influence this graph at first
};

