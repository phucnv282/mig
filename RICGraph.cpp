#include "RICGraph.h"
#include "Common.h"
#include <algorithm>
#include <iostream>

RICGraph::RICGraph(int communityId, int threshold, vector<int> *communityNodeIds) {
    this->communityId = communityId;
    this->threshold = threshold;
    this->communityNodeIds = communityNodeIds;
}

RICGraph::~RICGraph() {

}

void RICGraph::addReachable(int nodeIds, vector<int> *reachNodeIds) {
    vector<int> reachable(*reachNodeIds);
    this->mapReachable.insert(std::pair<int, vector<int>>(nodeIds, reachable));
    for (int i = 0; i < reachable.size(); i++) {
        int nodeId = reachable[i];
        if (mapTouch.find(nodeId) != mapTouch.end()) {
            mapTouch[nodeId].push_back(nodeIds);
        } else {
            vector<int> tmp;
            tmp.push_back(nodeIds);
            mapTouch.insert(pair<int, vector<int>>(nodeId, tmp));
        }
    }
}

map<int, vector<int>> *RICGraph::getMapReachable() {
    return &mapReachable;
}

int RICGraph::getPopulation() {
    return communityNodeIds->size();
}

vector<int> *RICGraph::getCommunityNodeIds() {
    return communityNodeIds;
}

vector<int> *RICGraph::getReachableIds(int nodeId) {
    return &(mapReachable[nodeId]);
}

int RICGraph::getThreshold() {
    return threshold;
}

int RICGraph::getCommunityId() {
    return communityId;
}

vector<int> RICGraph::getCurrentLive(vector<int> *sol) {
    /*
    vector<int> re;
    for (int i = 0; i < communityNodeIds->size(); i++) {
        int nodeId = (*communityNodeIds)[i];
        vector<int> reachable = mapReachable[nodeId];
        if (!Common::getInstance()->isIntersected(sol, &reachable))
            re.push_back(nodeId);
    }
    if (re.size() > communityNodeIds->size() - threshold)
        return re;
    else return vector<int>();
    */

    vector<int> re;
    vector<int> currentKill;
    for (int i = 0; i < sol->size(); i++) {
        int nodeId = (*sol)[i];
        if (mapTouch.find(nodeId) != mapTouch.end()) {
            vector<int> tmp = mapTouch[nodeId];
            for (int j = 0; j < tmp.size(); j++) {
                currentKill.push_back(tmp[j]);
            }
        }
    }
    for (int i = 0; i < communityNodeIds->size(); i++) {
        int nodeId = (*communityNodeIds)[i];
        if (find(currentKill.begin(), currentKill.end(), nodeId) == currentKill.end())
            re.push_back(nodeId);
    }
    if (re.size() > communityNodeIds->size() - threshold)
        return re;
    else return vector<int>();
}

/*
int RICGraph::getMarginalGain(int nodeId, vector<int>* infNodes)
{
	int gain = 0;

	if (infNodes->size() >= threshold) // if number of influenced nodes exceed threshold, then no more gain
		return 0;

	if (mapTouch.find(nodeId) != mapTouch.end()) {
		vector<int> touch = mapTouch[nodeId];
		for (int i = currentLive->size() - 1; i >= 0; i--) {
			if (find(touch.begin(), touch.end(), (*currentLive)[i]) != touch.end()) {
				gain++;
				if (gain + communityNodeIds->size() - currentLive->size() >= threshold)
					break;
			}
		}
	}
	return gain;
}
*/

double RICGraph::getMarginalGainMig(int nodeId, vector<int> *currentLive, double *currentBenefit) {
    double gain = 0;
    // int gain = 0;

    if (*currentBenefit < totalBenefit - dThreshold)
        // if (currentLive->size() < communityNodeIds->size() - threshold)
        return 0;

    if (mapTouch.find(nodeId) != mapTouch.end()) {
        vector<int> touch = mapTouch[nodeId];
        for (int i = currentLive->size() - 1; i >= 0; i--) {
            int liveNode = currentLive->at(i);
            double liveBenefit = g->getNodeBenefit(liveNode);
            if (find(touch.begin(), touch.end(), liveNode) != touch.end()) {
                // if (find(touch.begin(), touch.end(), (*currentLive)[i]) != touch.end()) {
                gain += liveBenefit;
                // gain++;
                if (gain + (totalBenefit - *currentBenefit) >= dThreshold) {
                    // if (gain + communityNodeIds->size() - currentLive->size() >= threshold)
                    gain -= ((liveBenefit + (totalBenefit - *currentBenefit)) - dThreshold);
                    break;
                }
            }
        }
    }
    return gain;

    // double gain = 0.0;
    //
    // if (currentBenefit < totalBenefit - dThreshold)
    //     // if (currentLive->size() < communityNodeIds->size() - threshold)
    //     return 0.0;
    //
    // if (mapTouch.find(nodeId) != mapTouch.end()) {
    //     vector<int> touch = mapTouch[nodeId];
    //     for (int i = currentLive->size() - 1; i >= 0; i--) {
    //         int liveNode = (*currentLive)[i];
    //         if (find(touch.begin(), touch.end(), (*currentLive)[i]) != touch.end()) {
    //             gain += g->getNodeBenefit(liveNode);
    //             if (gain + totalBenefit - currentBenefit >= dThreshold)
    //                 break;
    //         }
    //     }
    // }
    // return gain;
}

int RICGraph::getMarginalGain(int nodeId, vector<int> *currentLive) {
    int gain = 0;

    if (currentLive->size() < communityNodeIds->size() - threshold)
        return 0;

    if (mapTouch.find(nodeId) != mapTouch.end()) {
        vector<int> touch = mapTouch[nodeId];
        for (int i = currentLive->size() - 1; i >= 0; i--) {
            if (find(touch.begin(), touch.end(), (*currentLive)[i]) != touch.end()) {
                gain++;
                if (gain + communityNodeIds->size() - currentLive->size() >= threshold)
                    break;
            }
        }
    }
    return gain;
}

void RICGraph::getCurrentLiveAfterAddingNodeMig(int nodeId, vector<int> *currentLive, double *currentBenefit) {
    if (*currentBenefit <= 0) return;
    if (mapTouch.find(nodeId) != mapTouch.end()) {
        vector<int> touch = mapTouch[nodeId];
        for (int i = currentLive->size() - 1; i >= 0; i--) {
            int liveNode = currentLive->at(i);
            double liveBenefit = g->getNodeBenefit(liveNode);
            if (find(touch.begin(), touch.end(), (*currentLive)[i]) != touch.end()) {
                currentLive->erase(currentLive->begin() + i);
                *currentBenefit -= liveBenefit;
                if (*currentBenefit <= totalBenefit - dThreshold) {
                    currentLive->clear();
                    *currentBenefit = 0;
                    return;
                }
            }
        }
    }
}

void RICGraph::getCurrentLiveAfterAddingNode(int nodeId, vector<int> *currentLive) {
    if (mapTouch.find(nodeId) != mapTouch.end()) {
        vector<int> touch = mapTouch[nodeId];
        for (int i = currentLive->size() - 1; i >= 0; i--) {
            if (find(touch.begin(), touch.end(), (*currentLive)[i]) != touch.end()) {
                currentLive->erase(currentLive->begin() + i);
                if (currentLive->size() <= communityNodeIds->size() - threshold) {
                    currentLive->clear();
                    return;
                }
            }
        }
    }
}

map<int, double>
RICGraph::updateGainAndCurrentLiveAfterAddingNodeMig(int nodeId, vector<int> *currentLive, double *currentBenefit) {
    map<int, double> re;
    if (*currentBenefit <= 0) return re;
    // if (currentLive->empty()) return re;

    if (mapTouch.find(nodeId) != mapTouch.end()) {
        vector<int> touch = mapTouch[nodeId];
        for (int i = currentLive->size() - 1; i >= 0; i--) {
            int nodeId = currentLive->at(i);
            if (find(touch.begin(), touch.end(), nodeId) != touch.end()) {
                currentLive->erase(currentLive->begin() + i);
                double liveBenefit = g->getNodeBenefit(nodeId);
                *currentBenefit -= liveBenefit;

                vector<int> reachable = mapReachable[nodeId];
                for (int j = 0; j < reachable.size(); j++) {
                    int tmp = reachable[j];
                    re[tmp] += liveBenefit;
                    dTrackGain[tmp] -= liveBenefit;

                    // gain -= ((liveBenefit + (totalBenefit - *currentBenefit)) - dThreshold);
                    if (*currentBenefit <= totalBenefit - dThreshold) { // if this graph is influenced
                        double refund = ((liveBenefit + (totalBenefit - *currentBenefit)) - dThreshold);
                        re[tmp] -= refund;
                        dTrackGain[tmp] += refund;
                    }
                    // re[tmp]++;
                    // dTrackGain[tmp]--;
                }

                if (*currentBenefit <= totalBenefit - dThreshold) { // if this graph is influenced
                    // if (currentLive->size() <= communityNodeIds->size() - threshold) { // if this graph is influenced
                    // reduce gain of all other node that touch remaining node of community in this dcr graph
                    for (map<int, double>::iterator it = dTrackGain.begin(); it != dTrackGain.end(); ++it) {
                        re[it->first] += it->second;
                    }
                    *currentBenefit = 0;
                    currentLive->clear();
                    return re;
                }
            }
        }

        double toInf = *currentBenefit - totalBenefit + dThreshold;
        // double toInf = currentLive->size() - communityNodeIds->size() + threshold;
        for (map<int, double>::iterator it = dTrackGain.begin(); it != dTrackGain.end(); ++it) {
            if (it->second > toInf) {
                re[it->first] += (it->second - toInf);
                dTrackGain[it->first] = toInf;
            }
        }
    }

    return re;
}

map<int, int> RICGraph::updateGainAndCurrentLiveAfterAddingNode(int nodeId, vector<int> *currentLive) {
    map<int, int> re;
    if (currentLive->empty()) return re;

    if (mapTouch.find(nodeId) != mapTouch.end()) {
        vector<int> touch = mapTouch[nodeId];
        for (int i = currentLive->size() - 1; i >= 0; i--) {
            int nodeId = currentLive->at(i);
            if (find(touch.begin(), touch.end(), nodeId) != touch.end()) {
                currentLive->erase(currentLive->begin() + i);
                vector<int> reachable = mapReachable[nodeId];

                for (int j = 0; j < reachable.size(); j++) {
                    int tmp = reachable[j];
                    re[tmp]++;
                    trackGain[tmp]--;
                }

                if (currentLive->size() <= communityNodeIds->size() - threshold) { // if this graph is influenced
                    // reduce gain of all other node that touch remaining node of community in this dcr graph
                    for (map<int, int>::iterator it = trackGain.begin(); it != trackGain.end(); ++it) {
                        re[it->first] += it->second;
                    }
                    currentLive->clear();
                    return re;
                }
            }
        }

        int toInf = currentLive->size() - communityNodeIds->size() + threshold;
        for (map<int, int>::iterator it = trackGain.begin(); it != trackGain.end(); ++it) {
            if (it->second > toInf) {
                re[it->first] += (it->second - toInf);
                trackGain[it->first] = toInf;
            }
        }
    }

    return re;
}

map<int, bool>
RICGraph::updateGainAndCurrentLiveAfterAddingNodeCG(int nodeId, vector<int> *currentLive, vector<int> *canKill) {
    map<int, bool> re;
    if (currentLive->empty()) return re;

    if (mapTouch.find(nodeId) != mapTouch.end()) {
        vector<int> touch = mapTouch[nodeId];
        for (int i = currentLive->size() - 1; i >= 0; i--) {
            int nodeId = currentLive->at(i);
            if (find(touch.begin(), touch.end(), nodeId) != touch.end()) {
                currentLive->erase(currentLive->begin() + i);

                if (currentLive->size() <= communityNodeIds->size() - threshold) {
                    currentLive->clear();
                    for (int j = 0; j < canKill->size(); j++)
                        re[canKill->at(j)] = false; // false if reduced gain
                    canKill->clear();
                    return re;
                }
            }
        }

        for (map<int, vector<int>>::iterator it = mapTouch.begin(); it != mapTouch.end(); it++) {
            int nodeId = it->first;
            if (find(canKill->begin(), canKill->end(), nodeId) == canKill->end()) {
                int count = 0;
                vector<int> kill = it->second;
                for (int i = 0; i < kill.size(); i++) {
                    if (find(currentLive->begin(), currentLive->end(), kill[i]) != currentLive->end()) {
                        count++;
                    }

                    if (communityNodeIds->size() - currentLive->size() + count >= threshold) {
                        canKill->push_back(nodeId);
                        re[nodeId] = true; // true if increasing gain
                        break;
                    }
                }
            }
        }
    }

    return re;
}

void RICGraph::updateInitalGainMig(map<int, double> *gain, map<int, double> *greedy, map<int, int> *mapDead,
                                   map<int, double> *mapInfMaf) {
    keyNode.clear();
    for (map<int, vector<int>>::iterator it = mapTouch.begin(); it != mapTouch.end(); ++it) {
        int nodeId = it->first;
        vector<int> touchNodes = it->second;
        int touch = it->second.size();
        double touchBenefit = 0.0;
        for (int i = 0; i < touchNodes.size(); ++i) {
            touchBenefit += g->getNodeBenefit(touchNodes[i]);
        }
        double tmp = touchBenefit > dThreshold ? dThreshold : touchBenefit;
        tmp = tmp / dThreshold;
        (*gain)[nodeId] += tmp;
        (*greedy)[nodeId] += tmp / g->getNodeCost(nodeId);

        if (touchBenefit >= dThreshold && mapDead != nullptr) {
            (*mapDead)[nodeId]++;
        }

        if (mapInfMaf != nullptr && mapReachable.find(nodeId) == mapReachable.end()) {
            (*mapInfMaf)[nodeId] += tmp;
        }

        if (touchBenefit >= dThreshold)
            keyNode.push_back(nodeId);
    }
}

void RICGraph::updateInitalGain(map<int, double> *gain, map<int, int> *mapDead, map<int, double> *mapInfMaf) {
    keyNode.clear();
    for (map<int, vector<int>>::iterator it = mapTouch.begin(); it != mapTouch.end(); ++it) {
        int nodeId = it->first;
        int touch = it->second.size();
        double tmp = touch > threshold ? threshold : touch;
        tmp = tmp / threshold;
        (*gain)[nodeId] += tmp;

        if (touch >= threshold && mapDead != nullptr) {
            (*mapDead)[nodeId]++;
        }

        if (mapInfMaf != nullptr && mapReachable.find(nodeId) == mapReachable.end()) {
            (*mapInfMaf)[nodeId] += tmp;
        }

        if (touch >= threshold)
            keyNode.push_back(nodeId);
    }
}

bool RICGraph::isKillMig(vector<int> *sol) {
    vector<int> kill;
    double killBenefit = 0;
    for (int i = 0; i < sol->size(); i++) {
        int nodeId = (*sol)[i];
        if (mapTouch.find(nodeId) != mapTouch.end()) {
            vector<int> tmp = mapTouch[nodeId];
            for (int j = 0; j < tmp.size(); j++) {
                if (find(kill.begin(), kill.end(), tmp[j]) == kill.end()) {
                    kill.push_back(tmp[j]);
                    killBenefit += g->getNodeBenefit(tmp[j]);
                    if (killBenefit >= dThreshold)
                        // if (kill.size() >= threshold)
                        return true;
                }
            }
        }
    }
    return false;
}

bool RICGraph::isKill(vector<int> *sol) {
    vector<int> kill;
    for (int i = 0; i < sol->size(); i++) {
        int nodeId = (*sol)[i];
        if (mapTouch.find(nodeId) != mapTouch.end()) {
            vector<int> tmp = mapTouch[nodeId];
            for (int j = 0; j < tmp.size(); j++) {
                if (find(kill.begin(), kill.end(), tmp[j]) == kill.end()) {
                    kill.push_back(tmp[j]);
                    if (kill.size() >= threshold)
                        return true;
                }
            }
        }
    }
    return false;
}

double RICGraph::fractionalInf(vector<int> *sol) {
    vector<int> kill;
    for (int i = 0; i < sol->size(); i++) {
        int nodeId = (*sol)[i];
        if (mapTouch.find(nodeId) != mapTouch.end()) {
            vector<int> tmp = mapTouch[nodeId];
            for (int j = 0; j < tmp.size(); j++) {
                if (find(kill.begin(), kill.end(), tmp[j]) == kill.end()) {
                    kill.push_back(tmp[j]);
                    if (kill.size() >= threshold)
                        return 1;
                }
            }
        }
    }
    return ((double) kill.size()) / threshold;
}

bool RICGraph::isTouchedByNode(int nodeId) {
    return (mapTouch.find(nodeId) != mapTouch.end());
}

vector<int> *RICGraph::getListTouchedNode() {
    vector<int> *list = new vector<int>();
    for (map<int, vector<int>>::iterator it = mapTouch.begin(); it != mapTouch.end(); ++it) {
        list->push_back(it->first);
    }
    return list;
}

vector<int> *RICGraph::getListTouchedNode(int excludeId) {
    vector<int> *list = new vector<int>();

    if (mapTouch.find(excludeId) == mapTouch.end() || mapTouch[excludeId].size() < threshold) {
        for (map<int, vector<int>>::iterator it = mapTouch.begin(); it != mapTouch.end(); ++it) {
            if (it->first != excludeId)
                list->push_back(it->first);
        }
    }

    return list;
}

void RICGraph::initiateTrackGainMig() {
    for (map<int, vector<int>>::iterator it = mapTouch.begin(); it != mapTouch.end(); ++it) {
        // double tmp = it->second.size();
        vector<int> touchNodes = it->second;
        double touchBenefit = 0;
        for (int i = 0; i < touchNodes.size(); ++i) {
            touchBenefit += g->getNodeBenefit(touchNodes[i]);
        }
        // tmp = tmp < threshold ? tmp : threshold;
        touchBenefit = touchBenefit < dThreshold ? touchBenefit : dThreshold;
        dTrackGain[it->first] = touchBenefit;
    }
}

void RICGraph::initiateTrackGain() {
    for (map<int, vector<int>>::iterator it = mapTouch.begin(); it != mapTouch.end(); ++it) {
        int tmp = it->second.size();
        tmp = tmp < threshold ? tmp : threshold;
        trackGain[it->first] = tmp;
    }
}

map<int, int> *RICGraph::getTrackGain() {
    return &trackGain;
}

vector<int> *RICGraph::getKeyNodes() {
    return &keyNode;
}

RICGraph::RICGraph(SocialGraph *g, int communityId, double dThreshold, vector<int> *communityNodeIds,
                   double totalBenefit) {
    this->g = g;
    this->communityId = communityId;
    // this->threshold = dThreshold;
    this->dThreshold = dThreshold;
    this->communityNodeIds = communityNodeIds;
    this->totalBenefit = totalBenefit;
}

double RICGraph::getDThreshold() {
    // return threshold;
    return dThreshold;
}



