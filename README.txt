Some algorithms for Influence Groups in Online Social Networks

Requirements:
--------------------------------------------------------
In order to compile all the sourcecode, it requires MSVC 14.27.29110 and later (which also support OpenMP).
CPLEX 12.10 is also required.

Compile:
--------------------------------------------------------
Use CMake to compile everything

How to use:
--------------------------------------------------------
Before running influence maximization algorithms, please format the graph first!
Execute the command: {your_exec} [options].

1. Standardize a social graph: Computing edge weights + Conversion to binary file
    {your_exec} -g=<graph_name> -dd=<data_directory> -m=0

    <data_directory>: directory where locate social graph file
    <graph_name>: graph name of social graph (<graph_name>.txt) that located in <data_directory> (each line contains a couple "src dest")

    * For OPIM: {your_exec} -g=<graph_name> -dd=<data_directory> -m=5 -f=1

2. Compute communities on graphs weighted:
    cd <src_directory>/gen-louvain
    make
    ./convert -i <dataDir>/<graph>.adj -o <dataDir>/<graph>.bin -w <dataDir>/<graph>.weights
    ./louvain <dataDir>/<graph>.bin -l -1 -w <dataDir>/<graph>.weights > <dataDir>/<graph>.tree
    ./hierarchy <dataDir>/<graph>.tree > <dataDir>/<graph>.cs

3. Run platform to find the seed sets for Influence Groups in Online Social Networks
    {your_exec} [options]

    options:
        -m (mode):
            Specify the function to calculate seed sets. Possible values:
                0: Format the graph (see above)
                1: EIG algorithm
                2: AIG algorithm
                3: UBG algorithm
                4: High Degree algorithm
                5: OPIM algorithm

        -g (graph):
            Specify the graph name to process

        -e (epsilon):
            Epsilon value
            Default: 0.2

        -d (delta):
            Delta value
            Default: 0.2

        -noc (number of communities):
            Specify number of communities which want to influence

        -npc (number of nodes per community):
            Specify number of node per community

        -tp (threshold percentage):
            Specify threshold percentage for influencing a community
            Default: 0.5

        -nc (normal cost):
            0: cost is 1
            1: normalized cost follow paper

        -rb (random benefit):
            0: benefit is 1
            1: benefit is random in (0, 1]

        -rt (random threshold percentage):
            0: use -tp value option as threshold percentage
            1: threshold percentage is random in (0, 1]

        -t (number threads):
            Specify number of process threads for OpenMP parallel programming
            Default: 10

        -o (output path for result record):
            Specify the path to the result record file
            Default: the same directory with {your_exec}