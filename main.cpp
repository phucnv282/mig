#include <iostream>
#include "SocialGraph.h"
#include "RICGenerator.h"
#include "UBG.h"
#include <omp.h>
#include <string>
#include "MIP.h"
#include "Formatter.h"
#include "AIG.h"
#include "HighDegree.h"
#include "OPIM.h"

typedef StopWatch StopWatch;
using namespace std;

int main(int argc, const char *argv[]) {
    StopWatch *sw = new StopWatch();
    const Argument arg(argc, argv);
    cout << "graph: " << arg.graph << endl;
    if (arg.mode == 0) { // generate edge weights
        Formatter *fd = new Formatter(arg);
        fd->run();
        return 0;
    }
    SocialGraph *g = new SocialGraph();
    omp_set_num_threads(arg.threads);

    if (arg.mode == 5 && arg.format > 0) {
        OPIM opim(arg, g);
        opim.formatFile();
        return 0;
    }

    Constant::NUM_THREAD = arg.threads;
    Constant::IS_BOUNDED_THRESHOLD = false;
    Constant::IS_WEIGHTED = false;
    Constant::COMMUNITY_POPULATION = arg.npc;
    Constant::K = 100;
    Constant::noc = arg.noc;
    Constant::npc = arg.npc;
    Constant::mode = arg.mode;

    vector<int> sol;
    sol.clear();

    double res = 0;
    string graphFile = arg.dataDir + arg.graph + ".graph";
    string commFile = arg.dataDir + arg.graph + ".cs";
    if (arg.mode == 3) {
        g->readSocialGraphFromFile(graphFile, commFile);
    } else {
        g->readSocialGraphFromFileMig(graphFile, commFile);
    }
    // return 0;
    cout << "#nodes: " << g->getNumberOfNodes() << " - #edges: " << g->getNumberOfEdges() << endl;
    cout << "#communities: " << g->getNumberOfCommunities() << endl;
    sw->start();
    if (arg.mode == 1) {
        cout << "MIP is running..." << endl;
        MIP mip(g);
        mip.getSolution(&sol, &res);
    } else if (arg.mode == 2) {
        cout << "AIG is running..." << endl;
        AIG aig(g);
        aig.getSolution(&sol, &res);
    } else if (arg.mode == 3) {
        cout << "UBG is running..." << endl;
        UBG ubg(g);
        int left = 1;
        int right = g->getNumberOfNodes();
        ubg.binarySearchSolution(&sol, &res, left, right);
    } else if (arg.mode == 4) {
        cout << "HighDegree is running..." << endl;
        HighDegree hd(g);
        hd.getSolution(&sol, &res);
    } else if (arg.mode == 5) {
        cout << "OPIM is running..." << endl;
        OPIM opim(arg, g);
        opim.getSolution(&sol, &res);
    }
    sw->stop();
    double endIn = sw->getSeconds();
    double cost = 0;
    if (arg.nc > 0) {
        for (int i = 0; i < sol.size(); ++i) {
            cost += g->getNodeCost(sol[i]);
        }
    } else {
        cost = sol.size();
    }
    cout << "------------------------" << endl;
    cout << "#seeds: " << sol.size() << endl;
    cout << "cost: " << cost << endl;
    cout << "runtime: " << endIn << "s" << endl;
    Common::recordResult(arg, (double) cost, endIn);
    // Common::printVector(sol);

    delete g;
    return 0;
}