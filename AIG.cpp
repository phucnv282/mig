#include "AIG.h"
#include <omp.h>
#include <iostream>
#include "HeapData.hpp"
#include "mappedheap.hpp"


AIG::AIG(SocialGraph *g) : Algorithm(g) {

}

AIG::~AIG() {
}

double AIG::getDeterministicSolutionSpeed(vector<int> *sol) {
    sol->clear();
    vector<int> *nodeIds = g->getListNodeIds();

    currentLive.clear();
    currentBenefit.clear();
    for (int i = 0; i < dcrSet.size(); i++) {
        RICGraph *dcr = dcrSet[i];
        vector<int> *commNodeIds = dcr->getCommunityNodeIds();
        currentLive.push_back(vector<int>(*commNodeIds));
        currentBenefit.push_back(g->getCommTotalBenefit(dcr->getCommunityId()));
        dcr->initiateTrackGainMig();
    }

    //#pragma omp parallel for
    vector<double> marginalGain(nodeIds->size(), 0);
    vector<double> greedy(nodeIds->size(), 0);
    for (int i = 0; i < nodeIds->size(); i++) {
        int u = (*nodeIds)[i];
        marginalGain[i] = intialGain[u];
        greedy[i] = initialGreedy[u];
    }

    InfCost<double> hd(&greedy[0]);
    MappedHeap<InfCost<double>> heap(indx, hd);
    // InfCost<double> hd(&marginalGain[0]);
    // MappedHeap<InfCost<double>> heap(indx, hd);

    double gain = 0.0;
    while ((gain * g->getNumberOfCommunities() / dcrSet.size()) < g->getNumberOfCommunities() - 1e-10) {
        // cout << (gain * g->getNumberOfCommunities() / dcrSet.size()) << " - " << g->getNumberOfCommunities() << endl;
        unsigned int maxInd = heap.pop();
        double maxGain = marginalGain[maxInd];
        gain += maxGain;
        if (maxGain > 1e-10) {
            // cout << maxGain << endl;
            sol->push_back((*nodeIds)[maxInd]);
            // update current live
#pragma omp parallel for
            for (int i = 0; i < dcrSet.size(); i++) {
                map<int, double> reducedGain = dcrSet[i]->updateGainAndCurrentLiveAfterAddingNodeMig((*nodeIds)[maxInd],
                                                                                                     &(currentLive[i]),
                                                                                                     &(currentBenefit[i]));
#pragma omp critical
                {
                    for (map<int, double>::iterator it = reducedGain.begin(); it != reducedGain.end(); ++it) {
                        double reduce = (((double) it->second) / dcrSet[i]->getDThreshold());
                        marginalGain[mapNodeIdx[it->first]] -= reduce;
                        greedy[mapNodeIdx[it->first]] -= reduce / g->getNodeCost(it->first);
                        // marginalGain[mapNodeIdx[it->first]] -= (((double) it->second) / dcrSet[i]->getThreshold());
                        heap.heapify(mapNodeIdx[it->first]);
                    }
                }

            }
        } else break;
    }

    return gain * (Constant::IS_WEIGHTED ? g->getNumberOfNodes() : g->getNumberOfCommunities()) / dcrSet.size();
}

/*official running*/
double AIG::getDeterministicSolution(vector<int> *sol) {
    sol->clear();
    vector<int> nodeIds(*(g->getListNodeIds()));
    currentLive.clear();
    currentBenefit.clear();
    for (int i = 0; i < dcrSet.size(); i++) {
        RICGraph *dcr = dcrSet[i];
        vector<int> *commNodeIds = dcr->getCommunityNodeIds();
        currentLive.push_back(vector<int>(*commNodeIds));
        currentBenefit.push_back(g->getCommTotalBenefit(dcr->getCommunityId()));
    }

    double gain = 0;
    while ((gain * g->getNumberOfCommunities() / dcrSet.size()) < g->getNumberOfCommunities()) {
        int maxIndex = 0;
        double maxGain = 0;

#pragma omp parallel for
        for (int i = 0; i < nodeIds.size(); i++) {
            int u = nodeIds[i];
            double marginalGain = getMarginalGainMig(u, sol);

#pragma omp critical
            {
                if (marginalGain > maxGain) {
                    maxIndex = i;
                    maxGain = marginalGain;
                }
            }
        }

        if (maxGain > 0) {
            sol->push_back(nodeIds[maxIndex]);
            gain += maxGain;

            // update current live
#pragma omp parallel for
            for (int i = 0; i < dcrSet.size(); i++) {
                dcrSet[i]->getCurrentLiveAfterAddingNodeMig(nodeIds[maxIndex], &(currentLive[i]), &(currentBenefit[i]));
            }
            nodeIds.erase(nodeIds.begin() + maxIndex);
        } else {
            break;
        }
    }
    return gain * g->getNumberOfCommunities() / dcrSet.size();
}

double AIG::getSolution(vector<int> *sol, double *est = 0) {
    initiate();
    omp_set_num_threads(Constant::NUM_THREAD);
    generateDCRgraphsMig((int) initR);
    for (int i = 0; i < 1; ++i) {
        sol->clear();
        getDeterministicSolutionSpeed(sol);
        // getDeterministicSolution(sol);
        *est = estimate(sol);
        double K = (double) g->getNumberOfCommunities();
        cout << "#seeds: " << sol->size() << " - #estimate: " << *est << endl;
        if (*est >= K - ep * K) {
            return 0;
        } else {
            generateDCRgraphsMig(dcrSet.size());
        }
    }
    clear();
    return 0;
}

double AIG::getSolution2Step(vector<int> *sol, double *est) {
    sol->clear();
    initiate();
    omp_set_num_threads(Constant::NUM_THREAD);
    generateDCRgraphs((int) rMax);
    double re = getDeterministicSolution(sol);
    *est = estimateInf(sol);
    clear();
    return (*est) / re;
}


double AIG::estimate(vector<int> *sol, double epsilon, double delta, int tMax) {
    double lamda = 0.72;
    double tmp = 4 * lamda * log(2 / delta) / (epsilon * epsilon);
    double lambda = 1 + (1 + epsilon) * tmp;

    int T = 0;
    double inf = 0.0;

#pragma omp parallel for
    for (int i = 0; i < tMax; i++) {
        RICGraph *g = gen.generateDCRgraph();
        double fr = g->fractionalInf(sol);

#pragma omp critical
        {
            dcrSet.push_back(g);
            g->updateInitalGain(&intialGain, &initialDead);
            if (tMax > 0) {
                T++;
                inf += fr;
                if (inf >= lambda) {
                    tMax = -1;
                }
            }
        }
    }

    return (tMax == -1 ? lambda * (Constant::IS_WEIGHTED ? g->getNumberOfNodes() : g->getNumberOfCommunities()) / T
                       : -1);
}

double AIG::getMarginalGainMig(int nodeId, vector<int> *sol) {
    double re = 0.0;
    for (int i = 0; i < dcrSet.size(); i++) {
        RICGraph *dcr = dcrSet[i];
        double gain = dcr->getMarginalGainMig(nodeId, &(currentLive[i]), &(currentBenefit[i]));
        re += gain / dcr->getDThreshold();
    }
    return re;
}

double AIG::getMarginalGain(int nodeId, vector<int> *sol) {
    double re = 0.0;
    for (int i = 0; i < dcrSet.size(); i++) {
        RICGraph *dcr = dcrSet[i];
        int gain = dcr->getMarginalGain(nodeId, &(currentLive[i]));
        re += ((double) gain) / dcr->getThreshold();
    }
    return re;
}

void AIG::initiate() {
    ep = Constant::EPSILON;
    delta = Constant::DELTA;
    int n = g->getNumberOfNodes();
    int kmax = n / 2;
    initR = ((2 + ((2 * ep) / 3)) / (ep * ep)) * log(1 / delta);
    unsigned long long nCkmax = Common::nCk(n, kmax);
    maxR = ((2 + ((2 * ep) / 3)) / (ep * ep)) * log((2 * nCkmax) / delta);
    iMax = ceil(log2(maxR / initR));
    delta1 = delta / (2 * iMax);
    c = log(1 / delta1);

    vector<int> *nodeIds = g->getListNodeIds();
    indx = vector<int>(nodeIds->size(), 0);
    for (int i = 0; i < nodeIds->size(); i++) {
        int u = (*nodeIds)[i];
        indx[i] = i;
        mapNodeIdx.insert(pair<int, int>(u, i));
    }
}

double AIG::estimate(vector<int> *sol) {
    double K = (double) g->getNumberOfCommunities();
    double T = (double) dcrSet.size();
    double killedRIC = 0.0;

#pragma omp parallel for
    for (int i = 0; i < dcrSet.size(); ++i) {
        bool isKilled = dcrSet[i]->isKill(sol);
        if (isKilled) {
#pragma omp critical
            {
                killedRIC += 1.0;
            }
        }
    }
    double hatSigma = (K / T) * killedRIC;

    double min1 = K - ((K * c) / (3 * T));
    double min2 = K + (K / T) * (((2 * c) / 3) - sqrt(((4 * c * c) / 9) + (2 * T * c * (hatSigma / K))));
    return min(min1, min2);
}
