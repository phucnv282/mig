#pragma once

#include "SocialGraph.h"
#include "RICGraph.h"

class RICGenerator {
public:
    RICGenerator();

    RICGenerator(SocialGraph *g);

    ~RICGenerator();

    void setSocialGraph(SocialGraph *g);

    RICGraph *generateDCRgraph();

    RICGraph *generateDCRgraphICMig();

private:
    SocialGraph *g;

    RICGraph *generateDCRgraphIC();

    RICGraph *generateDCRgraphLT();

    void dfs(int u, vector<int> *reachable, map<int, vector<int>> *mapNeighbors);

    Common *commonInstance;

};

