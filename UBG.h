#pragma once

#include "Algorithm.h"

class UBG : public Algorithm {
public:
    UBG(SocialGraph *g);

    ~UBG();

    double getDeterministicSolution(vector<int> *sol);

    double getDeterministicSolutionSpeed(vector<int> *sol);

    //double estimate(vector<int> * sol, double epsilon2, double delta, int tMax);
    double getSolution(vector<int> *sol, double *est);

    double binarySearchSolution(vector<int> *sol, double *est, int left, int right);

    double getSolution2Step(vector<int> *sol, double *est);

    double estimate(vector<int> *sol, double epsilon, double delta, int tMax);

    double getHatSigma(vector<int> seeds);

private:
    double getMarginalGain(int nodeId, vector<int> *sol);

    double hatSigma;
    vector<int> finalSeeds;
    double finalInfluenced;

    vector<vector<int>> currentLive; // store current live node in each dcr graph after each iteration in greedy
};

