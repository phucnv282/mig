#include "Constant.h"


Constant::Constant() {
}


Constant::~Constant() {
}

double Constant::PERCENTAGE_THRESHOLD = 0.5;
double Constant::EPSILON = 0.2;
double Constant::DELTA = 0.2;
int Constant::K = 100;
bool Constant::IS_BOUNDED_THRESHOLD = false;
bool Constant::IS_WEIGHTED = false;
int Constant::COMMUNITY_POPULATION = 15;
int Constant::NUM_THREAD = 10;
bool Constant::MODEL = false;
int Constant::mode = 1;
int Constant::noc = 10;
int Constant::npc = 15;
